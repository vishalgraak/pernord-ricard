package app.com.guala;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;



import static android.R.attr.name;
import static android.media.CamcorderProfile.get;
import static app.com.guala.MainActivity.arrayList;
import static app.com.guala.MainActivity.fifoLifo;
import static app.com.guala.MainActivity.grid1List;
import static app.com.guala.MainActivity.grid1adapter;
import static app.com.guala.MainActivity.grid2List;
import static app.com.guala.MainActivity.grid2adapter;
import static app.com.guala.MainActivity.grid3List;
import static app.com.guala.MainActivity.grid3adapter;
import static app.com.guala.MainActivity.grid4List;
import static app.com.guala.MainActivity.grid4adapter;
import static app.com.guala.MainActivity.grid5List;
import static app.com.guala.MainActivity.grid5adapter;
import static app.com.guala.MainActivity.grid6List;
import static app.com.guala.MainActivity.grid6adapter;
import static app.com.guala.MainActivity.grid7List;
import static app.com.guala.MainActivity.grid7adapter;
import static app.com.guala.MainActivity.grid8List;
import static app.com.guala.MainActivity.grid8adapter;
import static app.com.guala.MainActivity.gridDataList;
import static app.com.guala.MainActivity.lifoList;
import static app.com.guala.MainActivity.listView;
import static app.com.guala.MainActivity.ll_detail;
import static app.com.guala.MainActivity.rl_list;
import static app.com.guala.MainActivity.searchList;
import static app.com.guala.MainActivity.selectionVar;
import static app.com.guala.MainActivity.tv159;
import static app.com.guala.MainActivity.tv160;
import static app.com.guala.MainActivity.tv97;
import static app.com.guala.MainActivity.tv98;
import static app.com.guala.MainActivity.tvLotNo;
import static app.com.guala.MainActivity.tvName;
import static app.com.guala.MainActivity.tvQty;
import static app.com.guala.MainActivity.tvTotal;




/**
 * Created by Sys9 on 8/1/2017.
 */

public class GridAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    Context mContext;
    ArrayList<InventoryModel> all = new ArrayList<>();
    String type;
    View previousSelectedItem;
    static String rowTemp;
    int posTemp;
    InventoryModel inventoryModel, inventoryModelClicked;
    ViewGroup viewGroup;
    String gridName;
    int rack_no;
    String typeTemp;
    private int rack_temp;


    public GridAdapter(Context context, ArrayList<InventoryModel> all, String type) {
        this.mContext = context;
        this.all = all;
        this.type = type;
    }

    @Override
    public int getCount() {
        return all.size();
    }

    @Override
    public Object getItem(int position) {
        return all.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View v;

        if (convertView == null) {
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.grid_item, null);
        }
try {
    TextView txt = (TextView) convertView.findViewById(R.id.txt);
    final RelativeLayout ll_grid = (RelativeLayout) convertView.findViewById(R.id.ll_grid);
    final ImageView img = (ImageView) convertView.findViewById(R.id.img);
    String pos = String.valueOf(position);

    v = convertView;

    for (int i = 0; i < all.size(); i++) {
        if (all.get(i).getPosition() == position) {
            inventoryModel = all.get(i);

        }
    }

    rack_temp = Integer.parseInt(inventoryModel.rack_no);

    txt.setVisibility(View.INVISIBLE);

    setDefaultLayoutValues();

    if (selectionVar) {

        setLayoutValues();

        if (inventoryModel.isSelected()) {
            txt.setVisibility(View.VISIBLE);
        }

        if (fifoLifo == 1 || fifoLifo == 2) {

            MainActivity.ll_98.setBackgroundResource(R.drawable.rack_unselected);
            MainActivity.ll_97.setBackgroundResource(R.drawable.rack_unselected);
            MainActivity.ll_160.setBackgroundResource(R.drawable.rack_unselected);
            MainActivity.ll_159.setBackgroundResource(R.drawable.rack_unselected);


            tv98.setText("");
            tv97.setText("");
            tv160.setText("");
            tv159.setText("");

            for (int i = 0; i < lifoList.size(); i++) {
                if (lifoList.get(i).rack_no.equals(inventoryModel.getRack_no())) {
                    txt.setText(String.valueOf(lifoList.get(i).rack));
                }

                if (lifoList.get(i).rack_no.equals(String.valueOf("98"))) {
                    tv98.setText(String.valueOf(lifoList.get(i).rack));
                    MainActivity.ll_98.setBackgroundResource(R.drawable.rack_selected);

                } else if (lifoList.get(i).rack_no.equals(String.valueOf("97"))) {
                    tv97.setText(String.valueOf(lifoList.get(i).rack));
                    MainActivity.ll_97.setBackgroundResource(R.drawable.rack_selected);

                } else if (lifoList.get(i).rack_no.equals(String.valueOf("159"))) {
                    tv159.setText(String.valueOf(lifoList.get(i).rack));
                    MainActivity.ll_159.setBackgroundResource(R.drawable.rack_selected);

                } else if (lifoList.get(i).rack_no.equals(String.valueOf("160"))) {
                    tv160.setText(String.valueOf(lifoList.get(i).rack));
                    MainActivity.ll_160.setBackgroundResource(R.drawable.rack_selected);
                }

            }

            if (inventoryModel.getName().equals("Empty")) {
                txt.setText("");
            }

        } else {
            txt.setVisibility(View.VISIBLE);
            txt.setText(String.valueOf(inventoryModel.rack_no));
        }

    } else {
        txt.setVisibility(View.VISIBLE);
        txt.setText(String.valueOf(inventoryModel.rack_no));
    }


    ll_grid.setBackgroundResource(R.drawable.rack_unselected);

    if (rack_temp >= 0 && rack_temp < 49) {
        if (inventoryModel.name.equals("Empty")) {
            img.setBackgroundResource(0);
        } else {
            img.setBackgroundResource(R.drawable.package_0);
        }
    } else if (rack_temp >= 49 && rack_temp < 65) {
        if (inventoryModel.name.equals("Empty")) {
            img.setBackgroundResource(0);
        } else {
            img.setBackgroundResource(R.drawable.package_1);
        }
    } else if (rack_temp >= 65 && rack_temp < 189) {
        if (inventoryModel.name.equals("Empty")) {
            img.setBackgroundResource(0);
        } else {
            img.setBackgroundResource(R.drawable.package_2);
        }
    } else if (rack_temp >= 189 && rack_temp < 222) {
        if (inventoryModel.name.equals("Empty")) {
            img.setBackgroundResource(0);
        } else {
            img.setBackgroundResource(R.drawable.package_3);
        }
    }

    for (int i = 0; i < all.size(); i++) {
        if (all.get(i).getPosition() == position && all.get(i).isSelected()) {
            ll_grid.setBackgroundResource(R.drawable.rack_selected);
        }
    }


/*if(all.get(position).rack_no.equals(String.valueOf("160"))) {
    if (!all.get(position).name.equals("Empty")) {
        MainActivity.img160.setBackgroundResource(R.drawable.package_2);
    } else {
        MainActivity.img160.setBackgroundResource(0);
    }
}
        if(all.get(position).rack_no.equals(String.valueOf("159"))) {
            if (!all.get(position).name.equals("Empty")) {
                MainActivity.img159.setBackgroundResource(R.drawable.package_2);
            } else {
                MainActivity.img159.setBackgroundResource(0);
            }
        }
        if(all.get(position).rack_no.equals(String.valueOf("98"))) {
            if (!all.get(position).name.equals("Empty")) {
                MainActivity.img98.setBackgroundResource(R.drawable.package_2);
            } else {
                MainActivity.img98.setBackgroundResource(0);
            }
        }
        if(all.get(position).rack_no.equals(String.valueOf("97"))) {
            if (!all.get(position).name.equals("Empty")) {
                MainActivity.img97.setBackgroundResource(R.drawable.package_2);
            } else {
                MainActivity.img97.setBackgroundResource(0);
            }
        }*/

    ll_grid.setOnClickListener(new View.OnClickListener() {
        @SuppressLint("LongLogTag")
        @Override
        public void onClick(View v) {
            try {

                selectionVar = false;
                MainActivity.fifolifoOp = false;

                MainActivity.rl_list.setVisibility(View.GONE);
                MainActivity.ll_detail.setVisibility(View.VISIBLE);
                MainActivity.tvMessage.setVisibility(View.GONE);

                for (int i = 0; i < all.size(); i++) {
                    if (all.get(i).getPosition() == position) {
                        inventoryModelClicked = all.get(i);
                        rack_no = Integer.parseInt(inventoryModelClicked.rack_no);
                        // Toast.makeText(mContext, "You clicked rack " +rack_no, Toast.LENGTH_SHORT).show();

                    }
                }

                resetAllValues();
                searchList.clear();
                MainActivity.ll_98.setBackgroundResource(R.drawable.rack_unselected);
                MainActivity.ll_97.setBackgroundResource(R.drawable.rack_unselected);
                MainActivity.ll_160.setBackgroundResource(R.drawable.rack_unselected);
                MainActivity.ll_159.setBackgroundResource(R.drawable.rack_unselected);


                if (rack_no >= 1 && rack_no < 49) {
                    gridName = "grid1";
                    typeTemp = "row0";
                } else if (rack_no >= 49 && rack_no < 65) {
                    gridName = "grid2";
                    typeTemp = "row0";
                } else if (rack_no >= 65 && rack_no < 97) {
                    gridName = "grid3";
                    typeTemp = "row1";
                } else if (rack_no >= 99 && rack_no < 127) {
                    gridName = "grid4";
                    typeTemp = "row1";
                } else if (rack_no >= 127 && rack_no < 159) {
                    gridName = "grid5";
                    typeTemp = "row1";
                } else if (rack_no >= 161 && rack_no < 189) {
                    gridName = "grid6";
                    typeTemp = "row1";
                } else if (rack_no >= 189 && rack_no < 210) {
                    gridName = "grid7";
                    typeTemp = "row2";
                } else if (rack_no >= 210 && rack_no < 221) {
                    gridName = "grid8";
                    typeTemp = "row2";
                }


                if (inventoryModelClicked.name.equals("")) {
                    MainActivity.tvRack.setText("No details found");
                    tvName.setText("No details found");
                    MainActivity.tvLotNo.setText("No details found");
                    MainActivity.tvQty.setText("No details found");
                } else {
                    //     MainActivity.tvRack.setText("Rack No - " + inventoryModelClicked.rack_no);

                /* if( inventoryModelClicked.name.contains(";")){
                Log.d("Name contain Semicolon","");
                String[] productName=inventoryModelClicked.name.split(";");
                String[] productLotNo=inventoryModelClicked.lot_no.split(";");
                String[] productQuantity=inventoryModelClicked.qty.split(";");
                for(int i=0;i<productName.length;i++){
                    tvName.setText("Name: " + productName[i]);
                    tvLotNo.setText("Lot No: " + productLotNo[i]);
                    tvQty.setText("Qty: " + productQuantity[i]);
                }
            }else{*/
                    ArrayList<InventoryModel> modelArrayList = new ArrayList<>();
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (all.get(position).rack_no.equals(arrayList.get(i).rack_no)) {
                            modelArrayList.add(arrayList.get(i));
                            // Log.d("Name  does not contain Semicolon",all.get(position).rack_no+",,,,,,"+modelArrayList.get(i).rack_no+",,,"+modelArrayList.get(i).name);

                            Log.d("Name  does not contain Semicolon", all.get(position).rack_no + ",,,,,," + arrayList.get(i).rack_no + ",,," + arrayList.get(i).name);
                          /*Log.d("Name  does not contain Semicolon",all.get(position).rack_no+",,,,,,"+arrayList.get(i).rack_no+",,,"+arrayList.get(i).name);
                          MainActivity.tvRack.setText("Rack No - " + arrayList.get(i).rack_no);
                          tvName.setText("Name: " + arrayList.get(i).name);
                          tvLotNo.setText("Lot No: " + arrayList.get(i).lot_no);
                          tvQty.setText("Qty: " + arrayList.get(i).qty);*/
                        }
                    }
                    tvTotal.setVisibility(View.GONE);
                    rl_list.setVisibility(View.VISIBLE);
                    ll_detail.setVisibility(View.GONE);
                    ListAdapter mAdapter = new ListAdapter(mContext, modelArrayList, "no");
                    listView.setAdapter(mAdapter);
                   /* Log.d("Name  does not contain Semicolon","");
                    tvName.setText("Name: " + inventoryModelClicked.name);
                    tvLotNo.setText("Lot No: " + inventoryModelClicked.lot_no);
                    tvQty.setText("Qty: " + inventoryModelClicked.qty);*/
            /*}*/
                }


                if (typeTemp.equals("row0")) {

                    if (gridName.equals("grid1")) {

                        for (int i = 0; i < gridDataList.size(); i++) {
                            if (!gridDataList.get(i).getRack_no().equals(rack_no))
                                gridDataList.get(i).setSelected(false);
                        }

                        for (int i = 0; i < grid1List.size(); i++) {
                            if (grid1List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid1List.get(i).setSelected(true);
                            }
                        }
                        notifyData();

                    } else if (gridName.equals("grid2")) {

                        for (int i = 0; i < grid2List.size(); i++) {
                            if (grid2List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid2List.get(i).setSelected(true);
                            }
                        }
                        notifyData();
                    }
                } else if (typeTemp.equals("row1")) {
                    if (gridName.equals("grid3")) {

                        for (int i = 0; i < grid3List.size(); i++) {
                            if (grid3List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid3List.get(i).setSelected(true);
                            }
                        }
                        notifyData();
                    } else if (gridName.equals("grid4")) {

                        for (int i = 0; i < grid4List.size(); i++) {
                            if (grid4List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid4List.get(i).setSelected(true);
                            }
                        }
                        notifyData();
                    }

                    if (gridName.equals("grid5")) {
                        for (int i = 0; i < grid5List.size(); i++) {
                            if (grid5List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid5List.get(i).setSelected(true);
                            }
                        }

                        notifyData();
                    } else if (gridName.equals("grid6")) {
                        for (int i = 0; i < grid6List.size(); i++) {
                            if (grid6List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid6List.get(i).setSelected(true);
                            }
                        }
                        notifyData();
                    }

//                } else if (typeTemp.equals("row2")) {
//                    if (gridName.equals("grid5")) {
//                        for (int i = 0; i < grid5List.size(); i++) {
//                            if (grid5List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
//                                grid5List.get(i).setSelected(true);
//                            }
//                        }
//
//                        notifyData();
//                    } else if (gridName.equals("grid6")) {
//                        for (int i = 0; i < grid6List.size(); i++) {
//                            if (grid6List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
//                                grid6List.get(i).setSelected(true);
//                            }
//                        }
//                        notifyData();
//                    }

                } else if (typeTemp.equals("row2")) {
                    if (gridName.equals("grid7")) {
                        for (int i = 0; i < grid7List.size(); i++) {
                            if (grid7List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid7List.get(i).setSelected(true);
                            }
                        }
                        notifyData();
                    } else if (gridName.equals("grid8")) {
                        for (int i = 0; i < grid8List.size(); i++) {
                            if (grid8List.get(i).getRack_no().equals(String.valueOf(rack_no))) {
                                grid8List.get(i).setSelected(true);
                            }
                        }
                        notifyData();
                    }

                }


                rowTemp = type;
                viewGroup = parent;
                posTemp = position;
                previousSelectedItem = ll_grid;
                ll_grid.setBackgroundResource(R.drawable.rack_selected);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });
}catch (Exception e){
            e.printStackTrace();
}
        return convertView;
    }

    public static void resetAllValues() {
        for (int i = 0; i < grid1List.size(); i++) {
            if (grid1List.get(i).isSelected() == true) {
                grid1List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid2List.size(); i++) {
            if (grid2List.get(i).isSelected() == true) {
                grid2List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid3List.size(); i++) {
            if (grid3List.get(i).isSelected() == true) {
                grid3List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid4List.size(); i++) {
            if (grid4List.get(i).isSelected() == true) {
                grid4List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid5List.size(); i++) {
            if (grid5List.get(i).isSelected() == true) {
                grid5List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid6List.size(); i++) {
            if (grid6List.get(i).isSelected() == true) {
                grid6List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid7List.size(); i++) {
            if (grid7List.get(i).isSelected() == true) {
                grid7List.get(i).setSelected(false);
            }

        }
        for (int i = 0; i < grid8List.size(); i++) {
            if (grid8List.get(i).isSelected() == true) {
                grid8List.get(i).setSelected(false);
            }

        }

    }

    public static void notifyData() {
        grid1adapter.notifyDataSetChanged();
        grid2adapter.notifyDataSetChanged();
        grid3adapter.notifyDataSetChanged();
        grid4adapter.notifyDataSetChanged();
        grid5adapter.notifyDataSetChanged();
        grid6adapter.notifyDataSetChanged();
        grid7adapter.notifyDataSetChanged();
        grid8adapter.notifyDataSetChanged();

    }

    public void setLayoutValues() {

        MainActivity.ll_98.setBackgroundResource(R.drawable.rack_unselected);
        MainActivity.ll_97.setBackgroundResource(R.drawable.rack_unselected);
        MainActivity.ll_160.setBackgroundResource(R.drawable.rack_unselected);
        MainActivity.ll_159.setBackgroundResource(R.drawable.rack_unselected);


        if (searchList.size() > 0) {
            for (int i = 0; i < searchList.size(); i++) {
                if (searchList.get(i).getRack_no().equals(String.valueOf("97"))) {
                    MainActivity.ll_97.setBackgroundResource(R.drawable.rack_selected);
                   // Toast.makeText(mContext, "value found" + "97", Toast.LENGTH_SHORT).show();
                } else if (searchList.get(i).getRack_no().equals(String.valueOf("98"))) {
                    MainActivity.ll_98.setBackgroundResource(R.drawable.rack_selected);
                   // Toast.makeText(mContext, "value found" + "98", Toast.LENGTH_SHORT).show();

                } else if (searchList.get(i).getRack_no().equals(String.valueOf("159"))) {
                    MainActivity.ll_159.setBackgroundResource(R.drawable.rack_selected);
                   // Toast.makeText(mContext, "value found" + "159", Toast.LENGTH_SHORT).show();

                } else if (searchList.get(i).getRack_no().equals(String.valueOf("160"))) {
                    MainActivity.ll_160.setBackgroundResource(R.drawable.rack_selected);
                   // Toast.makeText(mContext, "value found" + "160", Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    public void setDefaultLayoutValues() {
        tv98.setText("98");
        tv97.setText("97");
        tv160.setText("160");
        tv159.setText("159");

    }
}

