package app.com.guala;

/**
 * Created by SWS-PC10 on 7/31/2017.
 */

public class InventoryModel {
    public String rack;
    public String rack_no;
    public String name;
    public String lot_no;
    public String qty;
    public boolean isSelected;
    public int position = 0;
    public int posGrid = 0;

    public int getPosGrid() {
        return posGrid;
    }

    public void setPosGrid(int posGrid) {
        this.posGrid = posGrid;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getRack() {
        return rack;
    }

    public void setRack(String rack) {
        this.rack = rack;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getRack_no() {
        return rack_no;
    }

    public void setRack_no(String rack_no) {
        this.rack_no = rack_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLot_no() {
        return lot_no;
    }

    public void setLot_no(String lot_no) {
        this.lot_no = lot_no;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }




}
