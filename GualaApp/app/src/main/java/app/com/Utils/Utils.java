package app.com.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.com.guala.MainActivity;

import static app.com.guala.MainActivity.READ_REQUEST_CODE;

/**
 * Created by SWS-PC10 on 01-Dec-17.
 */

public class Utils {
    Activity mActivity;
    public Utils(Activity activity){
        mActivity=activity;
    }
    public void AllowPermissions() {
        int hasPermission = ActivityCompat.checkSelfPermission(mActivity.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> permissions = new ArrayList<String>();
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(mActivity, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            mActivity.startActivityForResult(Intent.createChooser(intent,
                    "Choose File to Upload.."), READ_REQUEST_CODE);
        }
    }


}
