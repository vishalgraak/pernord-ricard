package app.com.guala;

import java.util.Comparator;

/**
 * Created by Sys9 on 8/8/2017.
 */

public class ComparatorLotNo implements Comparator<InventoryModel> {
    @Override
    public int compare(InventoryModel o1, InventoryModel o2) {
        return Math.abs(Integer.parseInt(o2.lot_no)) - Math.abs(Integer.parseInt(o1.lot_no));
    }
}
