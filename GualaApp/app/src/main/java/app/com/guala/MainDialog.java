package app.com.guala;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import static app.com.guala.MainActivity.arrayList;
import static app.com.guala.MainActivity.fifolifoOp;
import static app.com.guala.MainActivity.listView;
import static app.com.guala.MainActivity.ll_detail;
import static app.com.guala.MainActivity.rl_list;
import static app.com.guala.MainActivity.searchList;
import static app.com.guala.MainActivity.selectionVar;
import static app.com.guala.MainActivity.tvMessage;
import static app.com.guala.MainActivity.tvTotal;

/**
 * Created by SWS-PC10 on 01-Dec-17.
 */

public class MainDialog {
    Activity mActivity;
    public MainDialog(Activity activity){
        mActivity=activity;
    }

    public void enterPasswordDialog(final SharedPreferences preferences) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final android.app.Dialog dialog = new android.app.Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_password);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
       /* if (height <= 850) {
            lp.height = 500;
        } else if (height <= 1280) {
            lp.height = 780;
        } else if (height <= 1920) {
            lp.height = 1050;
        } else if (height <= 2560) {
            lp.height = 1280;
        }*/
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mActivity.finish();
            }
        });

        final EditText edt_pass = (EditText) dialog.findViewById(R.id.edt_pass);

        final Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pass = edt_pass.getText().toString();
                if (edt_pass.length() == 0) {
                    Snackbar.make(btn_ok, "Empty Password!", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else if (edt_pass.length() < 6 || !pass.equals(preferences.getString("password", null))) {
                    Snackbar.make(btn_ok, "Incorrect Password!", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                }

            }
        });

    }


    public void resetDialog(final SharedPreferences preferences) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final android.app.Dialog dialog = new android.app.Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_reset);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        if (height <= 850) {
            lp.height = 500;
        } else if (height <= 1280) {
            lp.height = 780;
        } else if (height <= 1920) {
            lp.height = 1050;
        } else if (height <= 2560) {
            lp.height = 1280;
        }
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

        //  final EditText edt_old_pass = (EditText) dialog.findViewById(R.id.edt_old_pass);
        final EditText edt_new_pass = (EditText) dialog.findViewById(R.id.edt_new_pass);
        final EditText edt_cnfrm_pass = (EditText) dialog.findViewById(R.id.edt_cnfrm_pass);
        final Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        final Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // String old_pass = edt_old_pass.getText().toString();
                String new_pass = edt_new_pass.getText().toString();
                String cnfrm_pass = edt_cnfrm_pass.getText().toString();

//                if (edt_old_pass.length() == 0 || edt_new_pass.length() == 0 || edt_cnfrm_pass.length() == 0) {
//                    Snackbar.make(btn_ok, "Empty Password!", BaseTransientBottomBar.LENGTH_SHORT).show();
//                } else if (edt_old_pass.length() < 6 || !old_pass.equals(preferences.getString("password", null))) {
//                    Snackbar.make(btn_ok, "Incorrect Password!", BaseTransientBottomBar.LENGTH_SHORT).show();
//                } else

                if (edt_new_pass.length() < 6 || edt_cnfrm_pass.length() < 6) {
                    Snackbar.make(btn_ok, "Incorrect Password!", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else if (!new_pass.equals(cnfrm_pass)) {
                    Snackbar.make(btn_ok, "New Password and Confirm Password does not match!", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("password", cnfrm_pass);
                    editor.commit();

                    dialog.dismiss();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
 /*   public void showProductNameDialog(AlertDialog.Builder builder,final String[] charSequences,
                                      String mtitle, final int value) {

        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(mtitle);

        fifolifoOp = true;

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        choice = which;

                        search_bar.setText("" + charSequences[which]);
                        search_bar.setSelection(search_bar.length());

                        ll_detail.setVisibility(View.GONE);
                        rl_list.setVisibility(View.VISIBLE);
                        query = charSequences[which];


                        for (int i = 1; i < arrayList.size(); i++) {

                            if (query.length() > 1) {
                                if (arrayList.get(i).name.toLowerCase().contains(query.toLowerCase())) {
                                    searchList.add(arrayList.get(i));
                                    //searchList.add(getDistinctList(arrayList.get(i),z));
                                }

                            }
                        }

                        for (int j = 0; j < arrayList.size(); j++) {
                            arrayList.get(j).setSelected(false);
                        }

                        for (int j = 0; j < arrayList.size(); j++) {
                            for (int k = 0; k < searchList.size(); k++) {
                                if (arrayList.get(j).rack_no.equals(searchList.get(k).getRack_no())) {
                                    arrayList.get(j).setSelected(true);
                                }
                            }

                        }
                        //setLayoutValues();
                        selectionVar = true;

                        tvMessage.setVisibility(View.GONE);
                        setValues();
                        tvTotal.setVisibility(View.VISIBLE);
                        tvTotal.setText("Total Racks: " + searchList.size());

                        ListAdapter adapter = new ListAdapter(mActivity, searchList, "no");
                        listView.setAdapter(adapter);

                        dialog.dismiss();

                        final StringBuilder sb = new StringBuilder();
                        for (final InventoryModel inventoryModel : searchList) {
                            sb.append("In Rack " + inventoryModel.rack_no + ",");
                        }
                        if (sound == 0) {
                            MainActivity.speakOut(sb.toString());
                        }

                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
*/


}
