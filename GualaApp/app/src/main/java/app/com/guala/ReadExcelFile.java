package app.com.guala;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import static app.com.guala.MainActivity.arrayList;
import static app.com.guala.MainActivity.gridDataList;

/**
 * Created by SWS-PC10 on 01-Dec-17.
 */

public class ReadExcelFile {
    // get data from the excel sheet and put in arraylist
    public void readXLSXFile(Context context, Uri path) throws IOException {
        InputStream ExcelFileToRead = context.getContentResolver().openInputStream(path);

        XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
//        XSSFWorkbook wb = new XSSFWorkbook(context.getResources().getAssets().open("pernard.xlsx"));
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFRow row;
        XSSFCell cell;

        Iterator rows = sheet.rowIterator();

        InventoryModel inventoryModel;
        String rackNo = "";
        while (rows.hasNext()) {
            row = (XSSFRow) rows.next();
            Iterator cells = row.cellIterator();
            inventoryModel = new InventoryModel();
            while (cells.hasNext()) {

                cell = (XSSFCell) cells.next();

                /**Get value from excel sheet  column wise*/
                getColumnIndex(cell, inventoryModel, row);

                inventoryModel.isSelected = false;
            }


                arrayList.add(inventoryModel);

                // check if 2 or more row contain same rack no and put data on gridview list


        }
    }

    private void getColumnIndex(XSSFCell cell, InventoryModel inventoryModel, XSSFRow row) {
        if (cell.getColumnIndex() == 0) {
            if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                System.out.print(cell.getStringCellValue() + " ");
                Log.d("ValuesFound", cell.getStringCellValue());
                inventoryModel.rack_no = cell.getStringCellValue();
            } else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                System.out.print(cell.getNumericCellValue() + " ");
                Log.d("ValuesFound", String.valueOf(cell.getNumericCellValue()));
                inventoryModel.rack_no = String.valueOf((int) cell.getNumericCellValue());
            }
        }
        if (cell.getColumnIndex() == 1) {
            if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                System.out.print(cell.getStringCellValue() + " ");
                Log.d("ValuesFound", cell.getStringCellValue());
                inventoryModel.name = cell.getStringCellValue();

            } else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                System.out.print(cell.getNumericCellValue() + " ");
                Log.d("ValuesFound", String.valueOf(cell.getNumericCellValue()));
                inventoryModel.name = String.valueOf((int) cell.getNumericCellValue());
            }
        }
        if (cell.getColumnIndex() == 2) {
            if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                System.out.print(cell.getStringCellValue() + " ");
                Log.d("ValuesFound", cell.getStringCellValue());
                inventoryModel.lot_no = (cell.getStringCellValue());
            } else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {

                if (HSSFDateUtil.isCellDateFormatted(row.getCell(2))) {

                    System.out.print(cell.getDateCellValue() + " ");
                    Log.d("ValuesFound", String.valueOf(cell.getDateCellValue()));
                    inventoryModel.lot_no = (String.valueOf(cell.getDateCellValue()));
                } else {
                    System.out.print(cell.getNumericCellValue() + " ");
                    Log.d("ValuesFound", String.valueOf(cell.getNumericCellValue()));
                    inventoryModel.lot_no = (String.valueOf((int) cell.getNumericCellValue()));
                }
//                        System.out.print(cell.getNumericCellValue() + " ");
//                        Log.d("ValuesFound", String.valueOf(cell.getNumericCellValue()));
//                        inventoryModel.lot_no = (String.valueOf((int) cell.getNumericCellValue()));
            }
        }
        if (cell.getColumnIndex() == 3) {
            if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                System.out.print(cell.getStringCellValue() + " ");
                Log.d("ValuesFound", cell.getStringCellValue());
                inventoryModel.qty = (cell.getStringCellValue());
            } else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {

                if (HSSFDateUtil.isCellDateFormatted(row.getCell(3))) {
                    System.out.print(cell.getDateCellValue() + " ");
                    Log.d("ValuesFound", String.valueOf(cell.getDateCellValue()));
                    inventoryModel.qty = (String.valueOf(cell.getDateCellValue()));
                } else {
                    System.out.print(cell.getNumericCellValue() + " ");
                    Log.d("ValuesFound", String.valueOf(cell.getNumericCellValue()));
                    inventoryModel.qty = (String.valueOf((int) cell.getNumericCellValue()));
                }

//                        System.out.print(cell.getNumericCellValue() + " ");
//                        Log.d("ValuesFound", String.valueOf(cell.getNumericCellValue()));
//                        inventoryModel.qty = (String.valueOf((int) cell.getNumericCellValue()));
            }
        }


    }
}
