package app.com.guala;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import static android.R.attr.name;

/**
 * Created by SWS-PC10 on 5/30/2017.
 */

public class ListAdapter extends ArrayAdapter<InventoryModel> {

    Context context;
    ArrayList<InventoryModel> data;
    TextView tvRackNo, tvRack, tvName, tvLotNo, tvQty;
    String isNo;

    public ListAdapter(Context context, ArrayList<InventoryModel> data, String isNo) {

        super(context, R.layout.list_row, data);
        this.context = context;
        this.data = data;
        this.isNo = isNo;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final InventoryModel lab = data.get(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_row, parent, false);
        }

        tvRackNo = (TextView) convertView.findViewById(R.id.tvRackNo);
        tvRack = (TextView) convertView.findViewById(R.id.tvRack);
        tvName = (TextView) convertView.findViewById(R.id.tvName);
        tvLotNo = (TextView) convertView.findViewById(R.id.tvLotNo);
        tvQty = (TextView) convertView.findViewById(R.id.tvQty);

        if (isNo.equals("fifo")) {
            tvRackNo.setVisibility(View.VISIBLE);
            int no = position + 1;
            tvRackNo.setText("" + no);
        } else if (isNo.equals("lifo")) {
            tvRackNo.setVisibility(View.VISIBLE);
            tvRackNo.setText("" + lab.rack);
        } else if (isNo.equals("quarentine")) {
            tvRackNo.setVisibility(View.VISIBLE);
            int no = position + 1;
            tvRackNo.setText("" + no);
        } else {
            tvRackNo.setVisibility(View.GONE);
        }

        tvRack.setText("Rack No: " + lab.rack_no);
        if (lab.name.equals("")) {
            tvName.setText("No details found");
            tvLotNo.setText("No details found");
            tvQty.setText("No details found");
        } else {
           /* if(lab.name.contains(";")){
                Log.d("Name contain Semicolon","");
                String[] productName=lab.name.split(";");
                String[] productLotNo=lab.lot_no.split(";");
                String[] productQuantity=lab.qty.split(";");
                for(int i=0;i<productName.length;i++){
                    tvName.setText("Name: " + productName[i]);
                    tvLotNo.setText("Lot No: " + productLotNo[i]);
                    tvQty.setText("Qty: " + productQuantity[i]);
                }
            }else{*/
                Log.d("Nam not contn semicolon","");
                tvName.setText("Name: " + lab.name);
                tvLotNo.setText("Lot No: " + lab.lot_no);
                tvQty.setText("Qty: " + lab.qty);
           /* }*/

        }

        return convertView;
    }

}
