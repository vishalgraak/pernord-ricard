package app.com.guala;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import app.com.Utils.Utils;

import static app.com.guala.GridAdapter.notifyData;
import static app.com.guala.GridAdapter.resetAllValues;

public class MainActivity extends AppCompatActivity implements
        TextToSpeech.OnInitListener {
    public static final int READ_REQUEST_CODE = 001;
    public static ArrayList<InventoryModel> arrayList = new ArrayList<>();
    public static ArrayList<InventoryModel> gridDataList = new ArrayList<>();
    public static ArrayList<InventoryModel> grid1List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid2List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid3List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid4List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid5List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid6List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid7List = new ArrayList<>();
    public static ArrayList<InventoryModel> grid8List = new ArrayList<>();
    public static ArrayList<InventoryModel> searchList = new ArrayList<>();
    public static ArrayList<InventoryModel> lifoList = new ArrayList<>();
    public static GridAdapter grid1adapter, grid2adapter, grid3adapter, grid4adapter, grid5adapter,
            grid6adapter, grid7adapter, grid8adapter;
    public static boolean selectionVar = false;
    public static int fifoLifo = 0;
    public static RelativeLayout ll_98, ll_97, ll_160, ll_159;
    public static TextView tvRack, tvName, tvLotNo, tvQty;
    public static TextView tv98, tv97, tv160, tv159;
    public static boolean fifolifoOp = false;
    public static LinearLayout ll_details, ll_detail;
    public static RelativeLayout rl_list;
    public static ListView listView;
    public static TextView tvTotal;
    public static TextView tvMessage;
    public int z = 0;
    ImageView btn_sound;
    LinearLayout img_logo;
    Button btnSelectFile;

    ProgressDialog progressDialog;
    GridView grid1, grid2, grid3, grid4, grid5, grid6, grid7, grid8;
    ArrayList<InventoryModel> quarantineList = new ArrayList<>();
    ImageView img98, img97, img160, img159;
    LinearLayout ll_main;
    int count = 0, sound = 0;
    View view;
    ListAdapter adapter;

    int mile = -1;

    String[] arr = {"All", "Racks", "Name", "Row", "Lot No"};
    String[] name_arr;
    ArrayList<String> names = new ArrayList<>();
    private TextView btn_select;
    private EditText search_bar;
    private TextView tvNo, btn_fifo, btn_lifo, tvDateTime;
    private TextToSpeech tts;
    private AlertDialog.Builder builder;
    String query;
    int choice = -1;
    SharedPreferences preferences, pref;
private MainDialog mainDialog;
    private Utils mUtils;
    ReadExcelFile mReadExcelFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        init();

        onClicks();

        fifolifoOp = false;

    }

    private void setData() {
        if (arrayList.size() > 0) {
for(int i=1;i<arrayList.size();i++){

/*if(all.get(position).rack_no.equals(String.valueOf("160"))) {
    if (!all.get(position).name.equals("Empty")) {
        MainActivity.img160.setBackgroundResource(R.drawable.package_2);
    } else {
        MainActivity.img160.setBackgroundResource(0);
    }
}
        if(all.get(position).rack_no.equals(String.valueOf("159"))) {
            if (!all.get(position).name.equals("Empty")) {
                MainActivity.img159.setBackgroundResource(R.drawable.package_2);
            } else {
                MainActivity.img159.setBackgroundResource(0);
            }
        }
        if(all.get(position).rack_no.equals(String.valueOf("98"))) {
            if (!all.get(position).name.equals("Empty")) {
                MainActivity.img98.setBackgroundResource(R.drawable.package_2);
            } else {
                MainActivity.img98.setBackgroundResource(0);
            }
        }
        if(all.get(position).rack_no.equals(String.valueOf("97"))) {
            if (!all.get(position).name.equals("Empty")) {
                MainActivity.img97.setBackgroundResource(R.drawable.package_2);
            } else {
                MainActivity.img97.setBackgroundResource(0);
            }
        }*/
    if(arrayList.get(i).rack_no.equals(String.valueOf("98"))) {
        if (arrayList.get(i).name.equals("Empty")) {
            img98.setBackgroundResource(0);
        } else {
            img98.setBackgroundResource(R.drawable.package_2);
        }
    }
    if(arrayList.get(i).rack_no.equals(String.valueOf("97"))) {
        if (arrayList.get(i).name.equals("Empty")) {
            img97.setBackgroundResource(0);
        } else {
            img97.setBackgroundResource(R.drawable.package_2);
        }
    }
    if(arrayList.get(i).rack_no.equals(String.valueOf("160"))) {
        if (arrayList.get(i).name.equals("Empty")) {
            img160.setBackgroundResource(0);
        } else {
            img160.setBackgroundResource(R.drawable.package_2);
        }
    }
    if(arrayList.get(i).rack_no.equals(String.valueOf("159"))) {
        if (arrayList.get(i).name.equals("Empty")) {
            img159.setBackgroundResource(0);
        } else {
            img159.setBackgroundResource(R.drawable.package_2);
        }
    }
        }
    }
    }

    private void onClicks() {
        try {
            img_logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count = count + 1;
                    if (count == 14) {
                    /*Reset Password in the shared preferences*/
                        mainDialog.resetDialog(preferences);
                        count = 0;
                    }

                }
            });

            btnSelectFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        mUtils.AllowPermissions();
                    } else {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("*/*");
                        startActivityForResult(Intent.createChooser(intent,
                                "Choose File to Upload.."), READ_REQUEST_CODE);
                    }
                }
            });
/*Sort the arrayList in ascending order on the basis of lot no*/
            btn_fifo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fifolifoOp) {
                        if (searchList.size() > 0) {
                            lifoList.clear();
                            for (int i = 0; i < searchList.size(); i++) {
                                if (!searchList.get(i).lot_no.equals("Empty")) {
                                    lifoList.add(searchList.get(i));
                                }
                            }

                            Collections.sort(lifoList, new ComparatorLotNo());
                            Collections.reverse(lifoList);
                            for (int i = 0; i < lifoList.size(); i++) {
                                lifoList.get(i).setRack(String.valueOf(i + 1));
                            }
                            adapter = new ListAdapter(MainActivity.this, lifoList, "fifo");
                            listView.setAdapter(adapter);
                        } else {
                            ll_detail.setVisibility(View.GONE);
                            rl_list.setVisibility(View.VISIBLE);

                            for (int i = 1; i < arrayList.size(); i++) {
                                if (!arrayList.get(i).lot_no.equals("Empty")) {
                                    searchList.add(arrayList.get(i));
                                    //   searchList.add(getDistinctList(arrayList.get(i),z));
                                }
                            }

                            Collections.sort(searchList, new ComparatorLotNo());
                            Collections.reverse(searchList);
                            adapter = new ListAdapter(MainActivity.this, searchList, "fifo");
                            listView.setAdapter(adapter);
                        }
                        selectionVar = true;
                        fifoLifo = 1;
                        grid1adapter.notifyDataSetChanged();
                        grid2adapter.notifyDataSetChanged();
                        grid3adapter.notifyDataSetChanged();
                        grid4adapter.notifyDataSetChanged();
                        grid5adapter.notifyDataSetChanged();
                        grid6adapter.notifyDataSetChanged();
                        grid7adapter.notifyDataSetChanged();
                        grid8adapter.notifyDataSetChanged();
                    }
                }
            });
/*Sort the arrayList in descending order on the basis of lot no*/
            btn_lifo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fifolifoOp) {
                        if (searchList.size() > 0) {
                            lifoList.clear();
                            for (int i = 0; i < searchList.size(); i++) {
                                if (!searchList.get(i).lot_no.equals("Empty")) {
                                    lifoList.add(searchList.get(i));
                                }
                            }

                            Collections.sort(lifoList, new ComparatorLotNo());
                            int no = lifoList.size();
                            for (int j = 0; j < lifoList.size(); j++) {
                                lifoList.get(j).rack = "" + no;
                                no--;
                            }
                            adapter = new ListAdapter(MainActivity.this, lifoList, "lifo");
                            listView.setAdapter(adapter);
                        } else {
                            ll_detail.setVisibility(View.GONE);
                            rl_list.setVisibility(View.VISIBLE);

                            for (int i = 1; i < arrayList.size(); i++) {
                                if (!arrayList.get(i).lot_no.equals("Empty")) {
                                    searchList.add(arrayList.get(i));

                                    //  searchList.add(getDistinctList(arrayList.get(i),z));
                                }
                            }

                            Collections.sort(searchList, new ComparatorLotNo());
                            int no = searchList.size();
                            for (int j = 0; j < searchList.size(); j++) {
                                searchList.get(j).rack = "" + no;
                                no--;
                            }
                            adapter = new ListAdapter(MainActivity.this, searchList, "lifo");
                            listView.setAdapter(adapter);
                        }
                        selectionVar = true;
                        fifoLifo = 2;
                        grid1adapter.notifyDataSetChanged();
                        grid2adapter.notifyDataSetChanged();
                        grid3adapter.notifyDataSetChanged();
                        grid4adapter.notifyDataSetChanged();
                        grid5adapter.notifyDataSetChanged();
                        grid6adapter.notifyDataSetChanged();
                        grid7adapter.notifyDataSetChanged();
                        grid8adapter.notifyDataSetChanged();
                    }
                }
            });
/*Mute or unmute the voice*/
            btn_sound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sound == 0) {
                        sound = 1;
                        btn_sound.setImageResource(R.mipmap.mute);
                        tts.stop();
                    } else {
                        btn_sound.setImageResource(R.mipmap.unmute);
                        sound = 0;
                    }
                }
            });

            ll_98.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MainActivity.rl_list.setVisibility(View.GONE);
                    MainActivity.ll_detail.setVisibility(View.VISIBLE);

                    tvMessage.setVisibility(View.GONE);
                    MainActivity.fifolifoOp = false;

                    tv98.setText("98");

// Check if name is emmpty then set the background of relative
                    if (arrayList.get(98).name.equals("")) {
                        img98.setBackgroundResource(0);
                        ll_98.setBackgroundResource(R.drawable.rack_selected);
                        ll_97.setBackgroundResource(R.drawable.rack_unselected);
                        ll_160.setBackgroundResource(R.drawable.rack_unselected);
                        ll_159.setBackgroundResource(R.drawable.rack_unselected);

                        tvRack.setText("No details found");
                        tvName.setText("No details found");
                        tvLotNo.setText("No details found");
                        tvQty.setText("No details found");
                    } else {
                        //  img98.setBackgroundResource(R.drawable.package_2);
                        ll_98.setBackgroundResource(R.drawable.rack_selected);
                        ll_97.setBackgroundResource(R.drawable.rack_unselected);
                        ll_160.setBackgroundResource(R.drawable.rack_unselected);
                        ll_159.setBackgroundResource(R.drawable.rack_unselected);

                        int extra_rack = 98 - Integer.parseInt(arrayList.get(98).rack_no);

                        int add_rack = 98 + extra_rack;
                        tvRack.setText("Rack No - " + arrayList.get(add_rack).rack_no);
                        tvName.setText("Name - " + arrayList.get(add_rack).name);
                        tvLotNo.setText("Lot No - " + String.valueOf(arrayList.get(add_rack).lot_no));
                        tvQty.setText("Quantity - " + String.valueOf(arrayList.get(add_rack).qty));


                        for (int i = 0; i < arrayList.size(); i++) {
                            if (arrayList.get(i).getRack_no().equals("98")) {
                                arrayList.get(i).setSelected(true);
                            } else {
                                arrayList.get(i).setSelected(false);
                            }
                        }


                        selectionVar = false;
                        resetAllValues();
                        notifyData();
                        fifoLifo = 0;
                        ll_98.setBackgroundResource(R.drawable.rack_selected);
                    }
                }
            });

            ll_97.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MainActivity.rl_list.setVisibility(View.GONE);
                    MainActivity.ll_detail.setVisibility(View.VISIBLE);
                    tvMessage.setVisibility(View.GONE);
                    MainActivity.fifolifoOp = false;

                    tv97.setText("97");


                    if (arrayList.get(97).name.equals("")) {
                        img97.setBackgroundResource(0);
                        ll_97.setBackgroundResource(R.drawable.rack_selected);
                        ll_98.setBackgroundResource(R.drawable.rack_unselected);
                        ll_160.setBackgroundResource(R.drawable.rack_unselected);
                        ll_159.setBackgroundResource(R.drawable.rack_unselected);

                        tvRack.setText("No details found");
                        tvName.setText("No details found");
                        tvLotNo.setText("No details found");
                        tvQty.setText("No details found");
                    } else {
                        //  img97.setBackgroundResource(R.drawable.package_2);
                        ll_97.setBackgroundResource(R.drawable.item_selected);
                        ll_98.setBackgroundResource(R.drawable.rack_unselected);
                        ll_160.setBackgroundResource(R.drawable.rack_unselected);
                        ll_159.setBackgroundResource(R.drawable.rack_unselected);
                        int extra_rack = 97 - Integer.parseInt(arrayList.get(97).rack_no);
                        int add_rack = 97 + extra_rack;
                        tvRack.setText("Rack No - " + arrayList.get(add_rack).rack_no);
                        tvName.setText("Name - " + arrayList.get(add_rack).name);
                        tvLotNo.setText("Lot No - " + String.valueOf(arrayList.get(add_rack).lot_no));
                        tvQty.setText("Quantity - " + String.valueOf(arrayList.get(add_rack).qty));
                    }

                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getRack_no().equals("97")) {
                            arrayList.get(i).setSelected(true);
                        } else {
                            arrayList.get(i).setSelected(false);
                        }
                    }

                    selectionVar = false;
                    resetAllValues();
                    notifyData();
                    fifoLifo = 0;
                    ll_97.setBackgroundResource(R.drawable.rack_selected);
                }
            });

            ll_160.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MainActivity.rl_list.setVisibility(View.GONE);
                    MainActivity.ll_detail.setVisibility(View.VISIBLE);
                    tvMessage.setVisibility(View.GONE);
                    MainActivity.fifolifoOp = false;

                    tv160.setText("160");

                    if (arrayList.get(160).name.equals("")) {
                        img160.setBackgroundResource(0);
                        ll_160.setBackgroundResource(R.drawable.rack_selected);
                        ll_97.setBackgroundResource(R.drawable.rack_unselected);
                        ll_98.setBackgroundResource(R.drawable.rack_unselected);
                        ll_159.setBackgroundResource(R.drawable.rack_unselected);

                        tvRack.setText("No details found");
                        tvName.setText("No details found");
                        tvLotNo.setText("No details found");
                        tvQty.setText("No details found");
                    } else {
                        //  img160.setBackgroundResource(R.drawable.package_2);
                        ll_159.setBackgroundResource(R.drawable.rack_unselected);
                        ll_160.setBackgroundResource(R.drawable.rack_selected);
                        ll_97.setBackgroundResource(R.drawable.rack_unselected);
                        ll_98.setBackgroundResource(R.drawable.rack_unselected);
                        int extra_rack = 160 - Integer.parseInt(arrayList.get(160).rack_no);

                        int add_rack = 160 + extra_rack;


                        tvRack.setText("Rack No - " + arrayList.get(add_rack).rack_no);
                        tvName.setText("Name - " + arrayList.get(add_rack).name);
                        tvLotNo.setText("Lot No - " + String.valueOf(arrayList.get(add_rack).lot_no));
                        tvQty.setText("Quantity - " + String.valueOf(arrayList.get(add_rack).qty));
                    }

                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getRack_no().equals("160")) {
                            arrayList.get(i).setSelected(true);
                        } else {
                            arrayList.get(i).setSelected(false);
                        }
                    }

                    selectionVar = false;
                    resetAllValues();
                    notifyData();
                    fifoLifo = 0;

                }
            });

            ll_159.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv97.setText("159");
                    MainActivity.fifolifoOp = false;
                    MainActivity.rl_list.setVisibility(View.GONE);
                    MainActivity.ll_detail.setVisibility(View.VISIBLE);

                    tvMessage.setVisibility(View.GONE);
                    if (arrayList.get(159).name.equals("")) {
                        img159.setBackgroundResource(0);
                        ll_159.setBackgroundResource(R.drawable.rack_selected);
                        ll_160.setBackgroundResource(R.drawable.rack_unselected);
                        ll_97.setBackgroundResource(R.drawable.rack_unselected);
                        ll_98.setBackgroundResource(R.drawable.rack_unselected);
                        tvRack.setText("No details found");
                        tvName.setText("No details found");
                        tvLotNo.setText("No details found");
                        tvQty.setText("No details found");
                    } else {
                        //   img159.setBackgroundResource(R.drawable.package_2);
                        ll_159.setBackgroundResource(R.drawable.rack_selected);
                        ll_160.setBackgroundResource(R.drawable.rack_unselected);
                        ll_97.setBackgroundResource(R.drawable.rack_unselected);
                        ll_98.setBackgroundResource(R.drawable.rack_unselected);

                        int extra_rack = 159 - Integer.parseInt(arrayList.get(159).rack_no);

                        int add_rack = 159 + extra_rack;
                        tvRack.setText("Rack No - " + arrayList.get(add_rack).rack_no);
                        tvName.setText("Name - " + arrayList.get(add_rack).name);
                        tvLotNo.setText("Lot No - " + String.valueOf(arrayList.get(add_rack).lot_no));
                        tvQty.setText("Quantity - " + String.valueOf(arrayList.get(add_rack).qty));
                    }

                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getRack_no().equals("159")) {
                            arrayList.get(i).setSelected(true);
                        } else {
                            arrayList.get(i).setSelected(false);
                        }
                    }

                    selectionVar = false;
                    resetAllValues();
                    notifyData();
                    fifoLifo = 0;
                    ll_159.setBackgroundResource(R.drawable.rack_selected);
                }
            });


            search_bar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    try {
                        fifoLifo = 0;

                        if (actionId == EditorInfo.IME_ACTION_DONE) {

                            fifolifoOp = true;
                            for (int i = 1; i < arrayList.size(); i++) {
                        /*Search the data from array list on base
                        of Racks noumber and show it on Listview*/
                                if (btn_select.getText().toString().equals("Search by Racks")) {

                                    ll_detail.setVisibility(View.VISIBLE);
                                    rl_list.setVisibility(View.GONE);
                                    if (arrayList.get(i).rack_no != null) {

                                        selectionVar = true;
                                        if (arrayList.get(i).rack_no.equals(query)) {
                                            if (arrayList.get(i).name.equals("")) {
                                                tvRack.setText("No details found");
                                                tvName.setText("No details found");
                                                tvLotNo.setText("No details found");
                                                tvQty.setText("No details found");
                                            } else {
                                                tvRack.setText("Rack No: " + arrayList.get(i).rack_no);
                                                tvName.setText("Name: " + arrayList.get(i).name);
                                                tvLotNo.setText("Lot No: " + arrayList.get(i).lot_no);
                                                tvQty.setText("Qty: " + arrayList.get(i).qty);
                                                final String rack = arrayList.get(i).rack_no;

                                                for (int j = 0; j < arrayList.size(); j++) {
                                                    arrayList.get(j).setSelected(false);
                                                }

                                                if (searchList.size() >= 0) {
                                                    searchList.clear();
                                                }

                                                for (int j = 0; j < arrayList.size(); j++) {
                                                    if (arrayList.get(j).rack_no.equals(rack)) {
                                                        arrayList.get(j).setSelected(true);
                                                        searchList.add(arrayList.get(j));

                                                        //   searchList.add(getDistinctList(arrayList.get(j),z));
                                                    }
                                                }

                                                if (searchList.size() <= 0) {
                                            /*tvMessage.setVisibility(View.VISIBLE);*/
                                                } else {
                                                    tvTotal.setText("Total Racks: " + searchList.size());
                                                    tvMessage.setVisibility(View.GONE);
                                                }
                                                setValues();
                                                break;
                                            }
                                        }
                                    }
                            /*Search the data from array list on base
                        of Entire Row and show it on Listview*/
                                } else if (btn_select.getText().toString().equals("Search by Row")) {

                                    selectionVar = true;
                                    //tvMessage.setVisibility(View.GONE);
                                    if (query.toLowerCase().equals("0")) {

                                        for (int j = 0; j < grid1List.size(); j++) {
                                            searchList.add(grid1List.get(j));
                                            // searchList.add(getDistinctList(grid1List.get(j),z));
                                        }

                                        for (int j = 0; j < grid2List.size(); j++) {
                                            searchList.add(grid2List.get(j));
                                            // searchList.add(getDistinctList(grid2List.get(j),z));
                                        }

                                        break;
                                    } else if (query.toLowerCase().equals("1")) {

                                        for (int j = 0; j < grid3List.size(); j++) {
                                            searchList.add(grid3List.get(j));
                                            //  searchList.add(getDistinctList(grid3List.get(j),z));
                                        }


                                        for (int j = 0; j < grid4List.size(); j++) {
                                            searchList.add(grid4List.get(j));
                                            //  searchList.add(getDistinctList(grid4List.get(j),z));
                                        }
                                        for (int j = 0; j < grid5List.size(); j++) {
                                            searchList.add(grid5List.get(j));
                                            // searchList.add(getDistinctList(grid5List.get(j),z));
                                        }

                                        for (int j = 0; j < grid6List.size(); j++) {
                                            searchList.add(grid6List.get(j));
                                            //  searchList.add(getDistinctList(grid6List.get(j),z));
                                        }


                                        for (int j = 0; j < arrayList.size(); j++) {
                        /*Search the data from array list on base
                        of selected rack no and show it on Listview*/
                                            if (arrayList.get(j).getRack_no().equals(String.valueOf("98"))) {
                                                // searchList.add(getDistinctList(arrayList.get(j),z));
                                                searchList.add(arrayList.get(j));
                                            } else if (arrayList.get(j).getRack_no().equals(String.valueOf("97"))) {
                                                // searchList.add(getDistinctList(arrayList.get(j),z));

                                                searchList.add(arrayList.get(j));
                                            } else if (arrayList.get(j).getRack_no().equals(String.valueOf("160"))) {
                                                //searchList.add(getDistinctList(arrayList.get(j),z));
                                                searchList.add(arrayList.get(j));
                                            } else if (arrayList.get(j).getRack_no().equals(String.valueOf("159"))) {
                                                //   searchList.add(getDistinctList(arrayList.get(j),z));
                                                searchList.add(arrayList.get(j));
                                            }

                                        }

                                        break;
                                    } else if (query.toLowerCase().equals("2")) {

                                        for (int j = 0; j < grid7List.size(); j++) {
                                            searchList.add(grid7List.get(j));
                                            //  searchList.add(getDistinctList(grid7List.get(j),z));
                                        }


                                        for (int j = 0; j < grid8List.size(); j++) {
                                            searchList.add(grid8List.get(j));
                                            // searchList.add(getDistinctList(grid8List.get(i),z));

                                        }

                                        break;
                                    }

                                    if (searchList.size() <= 0) {
                                        btn_lifo.setEnabled(false);
                                        btn_fifo.setEnabled(false);
                                    } else {
                                        btn_lifo.setEnabled(true);
                                        btn_fifo.setEnabled(true);
                                    }
                                    final StringBuilder sb = new StringBuilder();
                                    for (final InventoryModel inventoryModel : searchList) {
                                        sb.append("In Rack " + inventoryModel.rack_no + ",");
                                    }

                                    if (sound == 0) {
                                        speakOut(sb.toString());
                                    }

/*Search the data from array list on base
                        of Name and show it on Listview*/
                                } else if (btn_select.getText().toString().equals("Search by Name")) {
                                    tvMessage.setVisibility(View.GONE);
                            /*Search the data from array list on base
                        of Lot number and show it on Listview*/
                                } else if (btn_select.getText().toString().equals("Search by Lot No")) {
                                    try {
                                        if (query.length() > 1) {

                                            if (!arrayList.get(i).getName().equals("Empty") && null != arrayList.get(i).lot_no) {
                                                if (Math.abs(Integer.parseInt(arrayList.get(i).lot_no.toLowerCase())) == Math.abs(Integer.parseInt(query.toLowerCase()))) {
                                                    searchList.add(arrayList.get(i));
                                                    //   searchList.add(getDistinctList(arrayList.get(i),z));
                                                }
                                            }

                                        }

                                        if (searchList.size() <= 0) {
                                            tvMessage.setVisibility(View.VISIBLE);
                                            rl_list.setVisibility(View.GONE);

                                            btn_lifo.setEnabled(false);
                                            btn_fifo.setEnabled(false);

                                            //   tvTotal.setText("");


                                            for (int r = 0; r < arrayList.size(); r++) {
                                                arrayList.get(r).setSelected(false);
                                            }
//                                setLayoutColors();
                                            setValues();

                                        } else {
                                            btn_lifo.setEnabled(true);
                                            btn_fifo.setEnabled(true);
                                            tvMessage.setVisibility(View.GONE);
                                            rl_list.setVisibility(View.VISIBLE);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            if (searchList.size() > 0) {
                                ll_detail.setVisibility(View.GONE);
                                rl_list.setVisibility(View.VISIBLE);
                                if (btn_select.getText().toString().equals("Search by Row")) {
                                    try {
                                        selectionVar = true;
                                        fifolifoOp = true;

                                        int qty = 0;
                                        for (int i = 0; i < searchList.size(); i++) {

                                            InventoryModel inventoryModel = searchList.get(i);
                                            Log.d("quantity inventory", "Quantity" + inventoryModel.qty);
                                            if (inventoryModel.qty != null && !inventoryModel.qty.equals("Empty")) {
                                                Log.d("quantity inventory", inventoryModel.qty);
                                                qty = qty + Integer.parseInt(inventoryModel.qty);
                                            }
                                        }
                                        tvTotal.setVisibility(View.VISIBLE);
                                        tvTotal.setText("Total Qty: " + qty);


                                        for (int j = 0; j < arrayList.size(); j++) {

                                            arrayList.get(j).setSelected(false);
                                            for (int k = 0; k < searchList.size(); k++) {
                                                if (arrayList.get(j).rack_no.equals(searchList.get(k).getRack_no())) {
                                                    arrayList.get(j).setSelected(true);
                                                }
                                            }

                                        }

                                        if (searchList.size() <= 0) {

                                            tvMessage.setVisibility(View.VISIBLE);
                                            rl_list.setVisibility(View.GONE);
                                        } else {
                                            tvMessage.setVisibility(View.GONE);
                                            rl_list.setVisibility(View.VISIBLE);
                                            btn_lifo.setEnabled(true);
                                            btn_fifo.setEnabled(true);
                                        }
                                        if (searchList.size() <= 0) {
                                            tvMessage.setVisibility(View.VISIBLE);
                                        } else {
                                            tvMessage.setVisibility(View.GONE);
                                        }
                                        setValues();
                                        // layoutValues();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (btn_select.getText().toString().equals("Search by Lot No")) {
                                    try {
                                        selectionVar = true;

                                        fifolifoOp = true;
                                        tvTotal.setVisibility(View.VISIBLE);
                                        tvTotal.setText("Total Racks: " + searchList.size());


                                        for (int j = 0; j < arrayList.size(); j++) {
                                            arrayList.get(j).setSelected(false);
                                        }

                                        for (int j = 0; j < arrayList.size(); j++) {
                                            for (int k = 0; k < searchList.size(); k++) {
                                                if (arrayList.get(j).rack_no.equals(searchList.get(k).getRack_no())) {
                                                    arrayList.get(j).setSelected(true);
                                                }
                                            }
                                        }

                                        if (searchList.size() <= 0) {
                                            tvMessage.setVisibility(View.VISIBLE);
                                            rl_list.setVisibility(View.GONE);
                                            for (int i = 0; i < arrayList.size(); i++) {
                                                arrayList.get(i).setSelected(false);
                                            }


                                        } else {
                                            tvMessage.setVisibility(View.GONE);
                                            rl_list.setVisibility(View.VISIBLE);
                                        }
                                        setValues();
                                        //layoutValues();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                adapter = new ListAdapter(MainActivity.this, searchList, "no");
                                listView.setAdapter(adapter);
                                final StringBuilder sb = new StringBuilder();
                                for (final InventoryModel inventoryModel : searchList) {
                                    sb.append("In Rack " + inventoryModel.rack_no + ",");
                                }
                                if (sound == 0) {
                                    speakOut(sb.toString());
                                }
                            }
                        }
                        if (searchList.size() <= 0) {
                            tvTotal.setVisibility(View.GONE);
                            tvMessage.setVisibility(View.VISIBLE);
                        } else {
                            tvMessage.setVisibility(View.GONE);
                        }
                      if(null==search_bar.getText().toString()){
                          tvMessage.setVisibility(View.GONE);
                      }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                }

            });

            search_bar.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    searchList.clear();
                    query = s.toString();
                    if (query.length() == 0) {
                        tvRack.setText("");
                        tvName.setText("");
                        tvLotNo.setText("");
                        tvQty.setText("");
                        searchList.clear();
                    }
                    for (int i = 1; i < arrayList.size(); i++) {
                        if (btn_select.getText().toString().equals("Search by All")) {
                            searchList.add(arrayList.get(i));
                            // searchList.add(getDistinctList(arrayList.get(i),z));
                        }
                    }
                    if (searchList.size() > 0) {
                        ll_detail.setVisibility(View.GONE);
                        rl_list.setVisibility(View.VISIBLE);

                        selectionVar = true;
                        fifolifoOp = true;

                        if (btn_select.getText().toString().equals("Search by All")) {
                            final StringBuilder sb = new StringBuilder();
                            for (final InventoryModel inventoryModel : searchList) {
                                sb.append("In Rack " + inventoryModel.rack_no + ",");
                            }
                            if (sound == 0) {
                                speakOut(sb.toString());
                            }
                        }
                        adapter = new ListAdapter(MainActivity.this, searchList, "no");
                        listView.setAdapter(adapter);
                        final StringBuilder sb = new StringBuilder();
                        for (final InventoryModel inventoryModel : searchList) {
                            sb.append("In Rack " + inventoryModel.rack_no + ",");
                        }
                        if (sound == 0) {
                            speakOut(sb.toString());
                        }

                    }
                }
            });

            btn_select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fifoLifo = 0;
                    int mile = -1;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search_bar.getWindowToken(), 0);
                    MyDialogSingle(arr, "Search By", mile);
                }
            });

            Log.d("StatusFlag", "onCreateCalled");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void init() {
        mReadExcelFile=new ReadExcelFile();
        mUtils=new Utils(MainActivity.this);

        tts = new TextToSpeech(MainActivity.this, MainActivity.this);
        view = (View) findViewById(R.id.view);
        mainDialog=new MainDialog(MainActivity.this);
        preferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        pref = getSharedPreferences("data", Context.MODE_PRIVATE);
        mainDialog.enterPasswordDialog(preferences);

        SharedPreferences.Editor editor = preferences.edit();

        String tempPass = pref.getString("password", "test");
        if (tempPass.equals("test")) {
            editor.putString("password", "123456");
            editor.commit();
        }
        img_logo = (LinearLayout) findViewById(R.id.img_logo_linear);
        btnSelectFile = (Button) findViewById(R.id.btnSelectFile);
        grid1 = (GridView) findViewById(R.id.grid1);
        grid2 = (GridView) findViewById(R.id.grid2);
        grid3 = (GridView) findViewById(R.id.grid3);
        grid4 = (GridView) findViewById(R.id.grid4);
        grid5 = (GridView) findViewById(R.id.grid5);
        grid6 = (GridView) findViewById(R.id.grid6);
        grid7 = (GridView) findViewById(R.id.grid7);
        grid8 = (GridView) findViewById(R.id.grid8);


        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        ll_details = (LinearLayout) findViewById(R.id.ll_details);
        ll_detail = (LinearLayout) findViewById(R.id.ll_detail);
        rl_list = (RelativeLayout) findViewById(R.id.rl_list);
        tvRack = (TextView) findViewById(R.id.tvRack);
        tvName = (TextView) findViewById(R.id.tvName);
        tvLotNo = (TextView) findViewById(R.id.tvLotNo);
        tvQty = (TextView) findViewById(R.id.tvQty);
        listView = (ListView) findViewById(R.id.listView);

        search_bar = (EditText) findViewById(R.id.search_bar);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);
        ll_98 = (RelativeLayout) findViewById(R.id.ll_98);
        ll_97 = (RelativeLayout) findViewById(R.id.ll_97);
        ll_160 = (RelativeLayout) findViewById(R.id.ll_160);
        ll_159 = (RelativeLayout) findViewById(R.id.ll_159);


        img98 = (ImageView) findViewById(R.id.img98);
        img97 = (ImageView) findViewById(R.id.img97);


        img159 = (ImageView) findViewById(R.id.img159);
        img160 = (ImageView) findViewById(R.id.img160);

        tv98 = (TextView) ll_98.findViewById(R.id.tv98);
        tv97 = (TextView) ll_97.findViewById(R.id.tv97);

        tv159 = (TextView) ll_159.findViewById(R.id.tv159);
        tv160 = (TextView) ll_160.findViewById(R.id.tv160);

        btn_select = (TextView) findViewById(R.id.btn_select);
        tvNo = (TextView) findViewById(R.id.tvNo);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvDateTime = (TextView) findViewById(R.id.tvDateTime);
        btn_fifo = (TextView) findViewById(R.id.btn_fifo);
        btn_lifo = (TextView) findViewById(R.id.btn_lifo);
        btn_sound = (ImageView) findViewById(R.id.btn_sound);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == READ_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri uri = data.getData();
                    String uriString = uri.getPath();
                    File file = new File(uriString);
                    String name = file.getName();
                    if (name.lastIndexOf(".") != -1 && name.lastIndexOf(".") != 0) {
                        String ext = name.substring(name.lastIndexOf(".") + 1);
                        if (!ext.equals("xlsx") && !ext.equals("xls")) {
                            name = file.getName() + ".xlsx";
                        }
                    } else {
                        name = file.getName() + ".xlsx";
                    }
                    //String name = new File(String.valueOf(data.getData())).getName();
                    Log.d("FileName", name);
                    if (name.endsWith("xlsx") || name.endsWith("xls")) {
                        arrayList.clear();
                        progressDialog.show();
                        mReadExcelFile.readXLSXFile(MainActivity.this, data.getData());
                        final Handler handler = new Handler();
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    Thread.sleep(2000);
                                } catch (Exception e) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Error is " + e, Toast.LENGTH_LONG).show();
                                }
                                handler.post(new Runnable() {
                                    public void run() {
                                        progressDialog.dismiss();
                                        clearLists();
                                        InventoryModel inventoryModel;
                                        if (arrayList.size() < 172) {
                                            for (int i = arrayList.size(); i < 172; i++) {
                                                inventoryModel = new InventoryModel();
                                                inventoryModel.rack_no = "" + i;
                                                inventoryModel.name = "";
                                                inventoryModel.lot_no = "";
                                                inventoryModel.qty = "";
                                                inventoryModel.isSelected = false;
                                                inventoryModel.position = i;
                                                arrayList.add(inventoryModel);
                                            }
                                        }
                                        if (arrayList.size() > 0) {

                                            btnSelectFile.setVisibility(View.GONE);
                                            ll_main.setVisibility(View.VISIBLE);
                                            view.setVisibility(View.VISIBLE);
                                            int pos = 0;

                                            String name = arrayList.get(1).getName();
                                            if (name.toUpperCase().equals("DATETIME")) {

                                                String dateTemp = "";
                                                try {
                                                    String date = arrayList.get(1).getLot_no();
                                                    DateFormat inputFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss zz yyyy");
                                                    //DateFormat inputFormat = new SimpleDateFormat("DD/MM/YYY");
                                                    Date d = inputFormat.parse(date);
                                                    DateFormat outputFormat = new SimpleDateFormat("MMM dd yyyy");
                                                    System.out.println(outputFormat.format(d));
                                                    dateTemp = outputFormat.format(d);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                    Log.d("dateError", e.getMessage());
                                                }


                                                String nameTemp = "Last Updated\n" + dateTemp;
                                                tvDateTime.setVisibility(View.VISIBLE);
                                                tvDateTime.setText(nameTemp);
                                                arrayList.remove(1);
                                            }


                                            setGridLists();

                                            grid1adapter = new GridAdapter(MainActivity.this, grid1List, "row0");
                                            grid1.setAdapter(grid1adapter);

                                            grid2adapter = new GridAdapter(MainActivity.this, grid2List, "row0");
                                            grid2.setAdapter(grid2adapter);

                                            grid3adapter = new GridAdapter(MainActivity.this, grid3List, "row1");
                                            grid3.setAdapter(grid3adapter);

                                            grid4adapter = new GridAdapter(MainActivity.this, grid4List, "row1");
                                            grid4.setAdapter(grid4adapter);

                                            grid5adapter = new GridAdapter(MainActivity.this, grid5List, "row1");
                                            grid5.setAdapter(grid5adapter);

                                            grid6adapter = new GridAdapter(MainActivity.this, grid6List, "row1");
                                            grid6.setAdapter(grid6adapter);

                                            grid7adapter = new GridAdapter(MainActivity.this, grid7List, "row2");
                                            grid7.setAdapter(grid7adapter);

                                            grid8adapter = new GridAdapter(MainActivity.this, grid8List, "row2");
                                            grid8.setAdapter(grid8adapter);
                                            setData();
                                        }
                                    }
                                });
                            }
                        }).start();
                    } else {
                        Toast.makeText(getApplicationContext(), "File format not supported!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    Log.e("Error", "" + e.getMessage());
                }
            }
        }
    }

    public void clearLists() {
        grid1List.clear();
        grid2List.clear();
        grid3List.clear();
        grid4List.clear();
        grid5List.clear();
        grid6List.clear();
        grid7List.clear();
        grid8List.clear();
    }



    /***** Show all options for search a listview******/
    public void MyDialogSingle(final String[] charSequences,
                               String mtitle, final int value) {
try{
        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(mtitle);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mile = which;
                        btn_select.setText("Search by " + charSequences[which]);

//                        actonType = which+1;
                        if (btn_select.getText().toString().equals("Search by Name")) {
                            dialog.dismiss();

                            for (int i = 1; i < arrayList.size(); i++) {
                                if (!arrayList.get(i).name.equals("Empty")) {
                                    names.add(arrayList.get(i).name);
                                }
                            }
                            name_arr = new String[names.size()];
                            for (int i = 0; i < names.size(); i++) {
                                name_arr[i] = names.get(i);
                            }
                            search_bar.setEnabled(false);

                            if (search_bar.requestFocus()) {
                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                            }

                            search_bar.setText("");
                            showProductNameDialog(name_arr, "Select Name", choice);


                        } else if (btn_select.getText().toString().equals("Search by All")) {

                            //selectionVar=true;

                            searchList.clear();

                            for (int i = 0; i < arrayList.size(); i++) {
                                String nameTemp = "Querentine";
                                if (!arrayList.get(i).getName().toUpperCase().equals(nameTemp.toUpperCase())
                                        && !arrayList.get(i).getName().equals("Empty")) {

                                    arrayList.get(i).setSelected(true);
                                    if (i != 0) {
                                        searchList.add(arrayList.get(i));
                                        // searchList.add(getDistinctList(arrayList.get(i),z));
                                    }
                                }
                            }

                            if (searchList.size() <= 0) {
                                tvMessage.setVisibility(View.VISIBLE);
                                rl_list.setVisibility(View.GONE);
                                ll_detail.setVisibility(View.GONE);
                                btn_lifo.setEnabled(false);
                                btn_fifo.setEnabled(false);
                            } else {
                                tvMessage.setVisibility(View.GONE);
                                rl_list.setVisibility(View.VISIBLE);
                                ll_detail.setVisibility(View.GONE);
                                btn_lifo.setEnabled(true);
                                btn_fifo.setEnabled(true);
                            }

                            selectionVar = true;
                            fifoLifo = 0;
                            fifolifoOp = true;

                            setValues();
                            //setLayoutColors();
                            tvTotal.setVisibility(View.VISIBLE);
                            tvTotal.setText("Total Racks: " + searchList.size());
                            adapter = new ListAdapter(MainActivity.this, searchList, "no");
                            listView.setAdapter(adapter);

                            final StringBuilder sb = new StringBuilder();
                            for (final InventoryModel inventoryModel : searchList) {
                                sb.append("In Rack " + inventoryModel.rack_no + ",");
                            }
                            if (sound == 0) {
                                speakOut(sb.toString());
                            }

                            dialog.dismiss();
                        } else {
                            search_bar.setEnabled(true);
                            setLayoutValues();
                            search_bar.setText("");
                            dialog.dismiss();
                        }
                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }catch(Exception e) {
    e.printStackTrace();
}
    }

    /***** Show all namefrom the main arraylist and then show listview of selected name******/
    public void showProductNameDialog(final String[] charSequences,
                                      String mtitle, final int value) {
try {
    builder = new AlertDialog.Builder(MainActivity.this);
    builder.setTitle(mtitle);

    fifolifoOp = true;

    builder.setSingleChoiceItems(charSequences, value,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    choice = which;
                    search_bar.setText("" + charSequences[which]);
                    search_bar.setSelection(search_bar.length());

                    ll_detail.setVisibility(View.GONE);
                    rl_list.setVisibility(View.VISIBLE);
                    query = charSequences[which];


                    for (int i = 1; i < arrayList.size(); i++) {

                        if (query.length() > 1) {
                            if (arrayList.get(i).name.toLowerCase().contains(query.toLowerCase())) {
                                searchList.add(arrayList.get(i));
                                //searchList.add(getDistinctList(arrayList.get(i),z));
                            }

                        }
                    }

                    for (int j = 0; j < arrayList.size(); j++) {
                        arrayList.get(j).setSelected(false);
                    }

                    for (int j = 0; j < arrayList.size(); j++) {
                        for (int k = 0; k < searchList.size(); k++) {
                            if (arrayList.get(j).rack_no.equals(searchList.get(k).getRack_no())) {
                                arrayList.get(j).setSelected(true);
                            }
                        }

                    }
                    //setLayoutValues();
                    selectionVar = true;

                    tvMessage.setVisibility(View.GONE);
                    setValues();
                    tvTotal.setVisibility(View.VISIBLE);
                    tvTotal.setText("Total Racks: " + searchList.size());

                    adapter = new ListAdapter(MainActivity.this, searchList, "no");
                    listView.setAdapter(adapter);

                    dialog.dismiss();

                    final StringBuilder sb = new StringBuilder();
                    for (final InventoryModel inventoryModel : searchList) {
                        sb.append("In Rack " + inventoryModel.rack_no + ",");
                    }
                    if (sound == 0) {
                        speakOut(sb.toString());
                    }

                }
            });

    builder.setNegativeButton("CANCEL",
            new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
    builder.show();
}catch (Exception e){
    e.printStackTrace();
}
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("*/*");
                      startActivityForResult(Intent.createChooser(intent,
                                "Choose File to Upload.."), READ_REQUEST_CODE);
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            // tts.setPitch(5); // set pitch level

            // tts.setSpeechRate(2); // set speech speed rate
//            Toast.makeText(getApplicationContext(), "Voice" + status, Toast.LENGTH_LONG).show();
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language is not supported");
            } else {
                speakOut("");
            }

        } else {
            Log.e("TTS", "Initilization Failed");
//            Toast.makeText(getApplicationContext(), "Voice" + status, Toast.LENGTH_LONG).show();
        }
    }




    private void speakOut(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    public void setValues() {

        setGridLists();
        grid1adapter.notifyDataSetChanged();
        grid2adapter.notifyDataSetChanged();
        grid3adapter.notifyDataSetChanged();
        grid4adapter.notifyDataSetChanged();
        grid5adapter.notifyDataSetChanged();
        grid6adapter.notifyDataSetChanged();
        grid7adapter.notifyDataSetChanged();
        grid8adapter.notifyDataSetChanged();

    }

/*put data in seprate grid list from
 main grid list to display it seprately*/
    @SuppressLint("LongLogTag")
    public void setGridLists() {
        try {
        if (grid1List.size() > 0) {
            grid1List.clear();
        }
        if (grid2List.size() > 0) {
            grid2List.clear();
        }
        if (grid3List.size() > 0) {
            grid3List.clear();
        }
        if (grid4List.size() > 0) {
            grid4List.clear();
        }
        if (grid5List.size() > 0) {
            grid5List.clear();
        }
        if (grid6List.size() > 0) {
            grid6List.clear();
        }
        if (grid7List.size() > 0) {
            grid7List.clear();
        }
        if (grid8List.size() > 0) {
            grid8List.clear();
        }


    gridDataList.clear();
        // check if 2 or more row contain same rack no and put data on gridview list
       String rackNo="";
       for(int i=0;i<arrayList.size();i++){
           if (!rackNo.equals(arrayList.get(i).rack_no)) {
               gridDataList.add(arrayList.get(i));
           }
           rackNo = arrayList.get(i).rack_no;
       }
Log.d("Check array list data:::::",arrayList+",,,"+gridDataList);



       /* gridDataList.addAll(arrayList);*/
    for (int j = 0; j <= 3; j++) {
        int n = 4 - j;
        int k = 0;
        for (int i = 1; i <= 12; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid1List.add(gridDataList.get(n + k));
            k = k + 4;
        }
    }

    for (int i = 0; i < grid1List.size(); i++) {
        grid1List.get(i).setPosition(i);
    }

    for (int j = 0; j <= 3; j++) {
        int n = 52 - j;
        int k = 0;
        for (int i = 1; i <= 4; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid2List.add(gridDataList.get(n + k));
            k = k + 4;
        }
    }

    for (int i = 0; i < grid2List.size(); i++) {
        grid2List.get(i).setPosition(i);
    }

    for (int j = 0; j <= 3; j++) {
        int n = 68 - j;
        int k = 0;
        for (int i = 1; i <= 8; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid3List.add(gridDataList.get(n + k));
            k = k + 4;
        }
    }

    for (int i = 0; i < grid3List.size(); i++) {
        grid3List.get(i).setPosition(i);
    }

    for (int j = 0; j <= 3; j++) {
        int n = 102 - j;
        int k = 0;
        for (int i = 1; i <= 7; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid4List.add(gridDataList.get(n + k));
            k = k + 4;
        }
    }

    for (int i = 0; i < grid4List.size(); i++) {
        grid4List.get(i).setPosition(i);
    }

    for (int j = 0; j <= 3; j++) {
        int n = 130 - j;
        int k = 0;
        for (int i = 1; i <= 8; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid5List.add(gridDataList.get(n + k));
            k = k + 4;
        }
    }

    for (int i = 0; i < grid5List.size(); i++) {
        grid5List.get(i).setPosition(i);
    }


    for (int j = 0; j <= 3; j++) {
        int n = 164 - j;
        int k = 0;
        for (int i = 1; i <= 7; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid6List.add(gridDataList.get(n + k));
            k = k + 4;
        }
    }

    for (int i = 0; i < grid6List.size(); i++) {
        grid6List.get(i).setPosition(i);
    }

    for (int j = 0; j <= 2; j++) {
        int n = 191 - j;
        int k = 0;
        for (int i = 1; i <= 7; i++) {
            if(n+k>=gridDataList.size()){
                break;
            }
            Log.d("Check array list data Rack Npo:::::",",,,"+gridDataList.get(n + k).getRack_no());
            grid7List.add(gridDataList.get(n + k));
            k = k + 3;
        }
    }

    for (int i = 0; i < grid7List.size(); i++) {
        grid7List.get(i).setPosition(i);
    }


    for (int j = 0; j <= 2; j++) {
        int n = 212 - j;
        int k = 0;
        for (int i = 1; i <= 4; i++) {
            if(n+k>gridDataList.size()){
                break;
            }
            grid8List.add(gridDataList.get(n + k));
            k = k + 3;
        }
    }

    for (int i = 0; i < grid8List.size(); i++) {
        grid8List.get(i).setPosition(i);
    }

}catch (Exception e){
    e.printStackTrace();
}
        }


    public void setLayoutValues() {

        ll_98.setBackgroundResource(R.drawable.rack_unselected);
        ll_97.setBackgroundResource(R.drawable.rack_unselected);
        ll_160.setBackgroundResource(R.drawable.rack_unselected);
        ll_159.setBackgroundResource(R.drawable.rack_unselected);


        for (int i = 0; i < searchList.size(); i++) {

            if (searchList.get(i).rack_no.equals(String.valueOf("98"))) {
                MainActivity.ll_98.setBackgroundResource(R.drawable.rack_selected);

            } else if (searchList.get(i).rack_no.equals(String.valueOf("97"))) {
                MainActivity.ll_97.setBackgroundResource(R.drawable.rack_selected);

            } else if (searchList.get(i).rack_no.equals(String.valueOf("160"))) {
                MainActivity.ll_160.setBackgroundResource(R.drawable.rack_selected);

            } else if (searchList.get(i).rack_no.equals(String.valueOf("159"))) {
                MainActivity.ll_159.setBackgroundResource(R.drawable.rack_selected);
            }
        }
    }

    /*private InventoryModel getDistinctList(InventoryModel model,int i) {
        InventoryModel inventoryModel;

        String mInventoryName = model.getName();
            if (mInventoryName.contains(";")) {
                Log.d("Name contain Semicolon", "");
                String[] productName = model.name.split(";");
                String[] productLotNo = model.lot_no.split(";");
                String[] productQuantity = model.qty.split(";");

                for ( i = 0; i < productName.length;i++) {
                    inventoryModel = new InventoryModel();
                    inventoryModel.rack_no = model.rack_no;
                    inventoryModel.name = productName[i];
                    inventoryModel.lot_no = productLotNo[i];
                    inventoryModel.qty = productQuantity[i];
                    Log.d("inventory Model check data", inventoryModel.rack_no + ",," + inventoryModel.name);
                  if(z==productName.length-1){
                      z=0;
                      return inventoryModel;
                  }else{
                      z++;
                  }

                }
            } else {
                Log.d("Name  does not contain Semicolon", "");
z=0;
                return model;
            }


       return  model;
    }*/


}
